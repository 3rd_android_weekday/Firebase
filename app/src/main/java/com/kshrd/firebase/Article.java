package com.kshrd.firebase;

/**
 * Created by pirang on 6/30/17.
 */

public class Article {

    private String id;
    private String title;
    private String imageUrl;

    public Article() {
    }

    public Article(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public Article(String id, String title, String imageUrl) {
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return title;
    }
}
