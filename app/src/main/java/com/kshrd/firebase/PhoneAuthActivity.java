package com.kshrd.firebase;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PhoneAuthActivity extends AppCompatActivity {

    @BindView(R.id.etVerificationCode)
    EditText etVerificationCode;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.btnVerify)
    Button btnVerify;
    @BindView(R.id.btnResend)
    Button btnResend;

    private Unbinder unbinder;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private FirebaseAuth auth;
    private String verificationId;
    private PhoneAuthProvider.ForceResendingToken token;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        unbinder = ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        fetchRemoteConfig();

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(PhoneAuthActivity.this, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(PhoneAuthActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                Toast.makeText(PhoneAuthActivity.this, "Code Sent", Toast.LENGTH_SHORT).show();
                PhoneAuthActivity.this.verificationId = verificationId;
                token = forceResendingToken;
            }
        };
    }

    private void fetchRemoteConfig() {
        long expiration = 3600;
        mFirebaseRemoteConfig.fetch(0)
            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(PhoneAuthActivity.this, "Fetch Succeeded",
                                Toast.LENGTH_SHORT).show();
                        mFirebaseRemoteConfig.activateFetched();
                    } else {
                        Toast.makeText(PhoneAuthActivity.this, "Fetch Failed", Toast.LENGTH_SHORT).show();
                    }
                    displayWelcomeMessage();
                }
            });
    }

    private void displayWelcomeMessage() {
        String title = mFirebaseRemoteConfig.getString("title");
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSend)
    public void onBtnSendClicked() {

        String phoneNumber = "+855" + etPhoneNumber.getText().toString();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,    // Phone number to verify
                60,     // Timeout Period
                TimeUnit.SECONDS,   // Unit of timeout
                this,
                callbacks
        );

    }

    @OnClick(R.id.btnVerify)
    public void onBtnVerifyClicked() {
        String code = etVerificationCode.getText().toString();
        verifyPhoneNumber(verificationId, code);
    }

    @OnClick(R.id.btnResend)
    public void onBtnResendClicked() {
        resendCode();
    }

    private void verifyPhoneNumber(String verificationId, String verificationCode) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, verificationCode);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = auth.getCurrentUser();
                    if (user != null){
                        user.delete();
                        auth.signOut();
                        Toast.makeText(PhoneAuthActivity.this, "Verify Success", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PhoneAuthActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void resendCode(){
        String phoneNumber = "+855" + etPhoneNumber.getText().toString();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                callbacks,
                token
        );
    }

}
